"use client";

import Image from "next/image";
import { useState, useEffect, useCallback } from "react";
import Navbar from "../components/navbar";
import Style from "./page.module.scss";
import { Poppins, Montserrat, DM_Serif_Display } from "next/font/google";
import Link from "next/link";
import AOS from "aos";
import "aos/dist/aos.css";

const poppins = Poppins({ subsets: ["latin"], weight: ["300", "700"] });
const montserrat = Montserrat({ subsets: ["latin"] });
const dmSherifDisplay = DM_Serif_Display({ subsets: ["latin"], weight: "400" });

export default function Home() {
  const [letterDeveloper, setLetterDeveloper] = useState();
  const [scrollY, setScrollY] = useState();

  const elemImage = document.getElementById("imagesProfile");
  const elemText = document.getElementById("textBackdrop");
  const itemArrow = document.getElementById("arrow");
  const itemName = document.getElementById("itsMyName");
  const aboutButton = document.getElementById("aboutMeButton");
  const sectionAbout = document.getElementById("sectionAbout");
  const contentFooter = document.getElementById("contentFooter");
  const wrapBefore = document.getElementById("wrapBefore");

  const writtingDeveloper = async () => {
    let letter = "Web Developer";
    let containerLetter = "";

    letter = letter.split("");
    let counter = 0;

    const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

    const pushLetter = async () => {
      containerLetter += letter[counter];
      counter++;
      setLetterDeveloper(containerLetter);
    };

    for (let i = 0; i < letter.length; i++) {
      if (counter == 2) {
        await pushLetter();
        await delay(1500);
      } else {
        await pushLetter();
        await delay(100);
      }
    }
  };

  const onScroll = useCallback((event) => {
    const { pageYOffset, scrollY } = window;
    setScrollY(pageYOffset);
  }, []);

  const scrollingAnimate = () => {
    elemImage?.animate([{ transform: `translateY(${scrollY / 10}px)` }], {
      duration: 100,
      timing: "ease-in-out",
      fill: "both",
    });

    elemText?.animate([{ paddingTop: `${scrollY / 10}px` }], {
      duration: 100,
      timing: "ease-in-out",
      fill: "both",
    });

    itemArrow?.animate([{ transform: `rotate(${scrollY / 10 + 45}deg)` }], {
      duration: 500,
      timing: "ease-in-out",
      fill: "both",
    });

    itemName?.animate(
      [{ transform: `translateX(calc(-30% + ${scrollY / 2 + 45}px))` }],
      {
        duration: 500,
        timing: "ease-in-out",
        fill: "both",
      }
    );

    aboutButton?.animate(
      [{ transform: `translateY(calc(-${scrollY / 10 - 50}px))` }],
      {
        duration: 500,
        timing: "ease-in-out",
        fill: "both",
      }
    );

    contentFooter?.animate(
      [{ transform: `translateY(calc(${scrollY / 5 - 150}px))` }],
      {
        duration: 500,
        timing: "ease-in-out",
        fill: "both",
      }
    );

    sectionAbout?.animate(
      [{ transform: `translateY(calc(-${scrollY / 10 - 20}px))` }],
      {
        duration: 500,
        timing: "ease-in-out",
        fill: "both",
      }
    );

    let radius = (50 - ((scrollY / 1605) * 100) / 2) * 5;

    if (radius > 0 && radius < 100) {
      wrapBefore?.animate(
        [
          {
            borderRadius: `${radius}%`,
          },
        ],
        {
          duration: 500,
          timing: "ease-in-out",
          fill: "both",
        }
      );
    }

    console.log(scrollY);
    console.log(`${radius}%`);
  };

  useEffect(() => {
    window.addEventListener("scroll", onScroll, { passive: true });
    return () => {
      window.removeEventListener("scroll", onScroll, { passive: true });
    };
  }, []);

  useEffect(() => {
    scrollingAnimate();
  }, [scrollY]);

  useEffect(() => {
    setTimeout(() => {
      writtingDeveloper();
    }, 1500);
  }, []);

  useEffect(() => {
    AOS.init({
      easing: "ease-out-cubic",
      once: true,
      offset: 50,
    });
  }, []);

  return (
    <>
      <Navbar />
      <section className={Style.hero}>
        <div className={Style.containerImage} id="imagesProfile">
          <Image
            src={"/images/profile.png"}
            width={600}
            height={774}
            alt="Profile Image"
            priority
          />
        </div>
        <div className={Style.containerText}>
          <h1
            className={`${Style.nameBackdrop} ${poppins.className}`}
            id="itsMyName"
          >
            Farhan Agung Maulana Farhan Agung Maulana&nbsp;
          </h1>
        </div>
        <h1
          id="textBackdrop"
          className={`${Style.textBehind} ${montserrat.className}`}
        >
          <i className="material-icons-outlined" id="arrow">
            arrow_back
          </i>
          Front-end Engineer
          <br /> PHP Engineer
          <span>{letterDeveloper}</span>
        </h1>
        <ul className={Style.socialIcons}>
          <li>
            <Link
              href="https://www.linkedin.com/in/farhanagungmaulana/"
              target="_blank"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="20"
                height="20"
                viewBox="0 0 20 20"
                fill="none"
              >
                <g clipPath="url(#clip0_2_321)">
                  <path
                    d="M18.5236 0H1.47639C1.08483 0 0.709301 0.155548 0.432425 0.432425C0.155548 0.709301 0 1.08483 0 1.47639V18.5236C0 18.9152 0.155548 19.2907 0.432425 19.5676C0.709301 19.8445 1.08483 20 1.47639 20H18.5236C18.9152 20 19.2907 19.8445 19.5676 19.5676C19.8445 19.2907 20 18.9152 20 18.5236V1.47639C20 1.08483 19.8445 0.709301 19.5676 0.432425C19.2907 0.155548 18.9152 0 18.5236 0ZM5.96111 17.0375H2.95417V7.48611H5.96111V17.0375ZM4.45556 6.1625C4.11447 6.16058 3.7816 6.05766 3.49895 5.86674C3.21629 5.67582 2.99653 5.40544 2.8674 5.08974C2.73826 4.77404 2.70554 4.42716 2.77336 4.09288C2.84118 3.7586 3.0065 3.4519 3.24846 3.21148C3.49042 2.97107 3.79818 2.80772 4.13289 2.74205C4.4676 2.67638 4.81426 2.71133 5.12913 2.84249C5.44399 2.97365 5.71295 3.19514 5.90205 3.47901C6.09116 3.76288 6.19194 4.09641 6.19167 4.4375C6.19488 4.66586 6.15209 4.89253 6.06584 5.104C5.97959 5.31547 5.85165 5.50742 5.68964 5.66839C5.52763 5.82936 5.33487 5.95607 5.12285 6.04096C4.91083 6.12585 4.68389 6.16718 4.45556 6.1625ZM17.0444 17.0458H14.0389V11.8278C14.0389 10.2889 13.3847 9.81389 12.5403 9.81389C11.6486 9.81389 10.7736 10.4861 10.7736 11.8667V17.0458H7.76667V7.49306H10.6583V8.81667H10.6972C10.9875 8.22917 12.0042 7.225 13.5556 7.225C15.2333 7.225 17.0458 8.22083 17.0458 11.1375L17.0444 17.0458Z"
                    fill="#526D82"
                  />
                </g>
                <defs>
                  <clipPath id="clip0_2_321">
                    <rect width="20" height="20" fill="white" />
                  </clipPath>
                </defs>
              </svg>
            </Link>
          </li>
          <li>
            <Link href="mailto:farhanagungmaulana@gmail.com" target="_blank">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="20"
                height="20"
                viewBox="0 0 20 20"
                fill="none"
              >
                <path
                  d="M15.006 0C15.691 0 16.3361 0.131422 16.9415 0.394265C17.5468 0.657109 18.0765 1.01553 18.5305 1.46953C18.9845 1.92354 19.3429 2.45321 19.6057 3.05854C19.8686 3.66388 20 4.30904 20 4.99403V14.9821C20 15.6671 19.8686 16.3162 19.6057 16.9295C19.3429 17.5428 18.9845 18.0765 18.5305 18.5305C18.0765 18.9845 17.5468 19.3429 16.9415 19.6057C16.3361 19.8686 15.691 20 15.006 20H4.99403C4.30904 20 3.66388 19.8686 3.05854 19.6057C2.45321 19.3429 1.92354 18.9845 1.46953 18.5305C1.01553 18.0765 0.657109 17.5428 0.394265 16.9295C0.131422 16.3162 0 15.6671 0 14.9821V4.99403C0 4.30904 0.131422 3.66388 0.394265 3.05854C0.657109 2.45321 1.01553 1.92354 1.46953 1.46953C1.92354 1.01553 2.45321 0.657109 3.05854 0.394265C3.66388 0.131422 4.30904 0 4.99403 0H15.006ZM4.37276 12.6165L7.38351 10.466L4.37276 8.31541V12.6165ZM15.6272 13.5484L11.9713 10.92L10.0119 12.3297L8.02867 10.92L4.37276 13.5484V14.9821H15.6272V13.5484ZM15.6272 8.31541L12.6165 10.466L15.6272 12.6165V8.31541ZM15.6272 4.99403H4.37276V6.45161L10.0119 10.466L15.6272 6.45161V4.99403Z"
                  fill="#526D82"
                />
              </svg>
            </Link>
          </li>
          <li>
            <Link href="https://wa.me/+6281336503277" target="_blank">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="20"
                height="20"
                viewBox="0 0 20 20"
                fill="none"
              >
                <path
                  d="M0 20L1.40583 14.8642C0.538332 13.3608 0.0824998 11.6567 0.0833332 9.90918C0.0858331 4.44585 4.53166 3.05176e-05 9.99414 3.05176e-05C12.645 0.000863849 15.1333 1.03336 17.005 2.90669C18.8758 4.78002 19.9058 7.27002 19.905 9.91834C19.9025 15.3825 15.4566 19.8283 9.99414 19.8283C8.33581 19.8275 6.70165 19.4117 5.25415 18.6217L0 20ZM5.49749 16.8275C6.89415 17.6567 8.22748 18.1533 9.99081 18.1542C14.5308 18.1542 18.2291 14.4592 18.2316 9.91668C18.2333 5.36502 14.5525 1.67503 9.99748 1.67336C5.45415 1.67336 1.75833 5.36835 1.75666 9.91001C1.75583 11.7642 2.29916 13.1525 3.21166 14.605L2.37916 17.645L5.49749 16.8275ZM14.9866 12.2742C14.925 12.1708 14.76 12.1092 14.5116 11.985C14.2641 11.8608 13.0466 11.2617 12.8191 11.1792C12.5925 11.0967 12.4275 11.055 12.2616 11.3033C12.0966 11.5508 11.6216 12.1092 11.4775 12.2742C11.3333 12.4392 11.1883 12.46 10.9408 12.3358C10.6933 12.2117 9.89498 11.9508 8.94915 11.1067C8.21331 10.45 7.71582 9.63918 7.57165 9.39084C7.42748 9.14334 7.55665 9.00918 7.67998 8.88584C7.79165 8.77501 7.92748 8.59668 8.05165 8.45168C8.17748 8.30835 8.21832 8.20501 8.30165 8.03918C8.38415 7.87418 8.34332 7.72918 8.28082 7.60501C8.21832 7.48168 7.72332 6.26252 7.51748 5.76668C7.31582 5.28419 7.11165 5.34919 6.95998 5.34169L6.48499 5.33335C6.31999 5.33335 6.05165 5.39502 5.82499 5.64335C5.59832 5.89168 4.95832 6.49002 4.95832 7.70918C4.95832 8.92834 5.84582 10.1058 5.96915 10.2708C6.09332 10.4358 7.71498 12.9375 10.1991 14.01C10.79 14.265 11.2516 14.4175 11.6108 14.5317C12.2041 14.72 12.7441 14.6933 13.1708 14.63C13.6466 14.5592 14.6358 14.0308 14.8425 13.4525C15.0491 12.8733 15.0491 12.3775 14.9866 12.2742Z"
                  fill="#526D82"
                />
              </svg>
            </Link>
          </li>
          <li>
            <Link href="https://gitlab.com/famaulana" target="_blank">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="20"
                height="20"
                viewBox="0 0 20 20"
                fill="none"
              >
                <g clipPath="url(#clip0_2_329)">
                  <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M10.0083 0C4.47396 0 0 4.50694 0 10.0826C0 14.5396 2.86662 18.3123 6.84338 19.6476C7.34058 19.748 7.5227 19.4306 7.5227 19.1637C7.5227 18.93 7.50631 18.1288 7.50631 17.2939C4.72225 17.895 4.14249 16.092 4.14249 16.092C3.69508 14.9235 3.03215 14.6232 3.03215 14.6232C2.12092 14.0055 3.09852 14.0055 3.09852 14.0055C4.1093 14.0723 4.63969 15.0405 4.63969 15.0405C5.53432 16.5761 6.97592 16.1422 7.55588 15.8751C7.63865 15.224 7.90394 14.7733 8.18563 14.523C5.96514 14.2893 3.62891 13.4213 3.62891 9.54836C3.62891 8.44662 4.02634 7.54523 4.65608 6.8442C4.55672 6.59386 4.20866 5.5587 4.75564 4.17322C4.75564 4.17322 5.60069 3.90608 7.5061 5.20818C8.32188 4.98747 9.16317 4.8752 10.0083 4.87426C10.8533 4.87426 11.7148 4.99123 12.5102 5.20818C14.4159 3.90608 15.2609 4.17322 15.2609 4.17322C15.8079 5.5587 15.4596 6.59386 15.3603 6.8442C16.0066 7.54523 16.3876 8.44662 16.3876 9.54836C16.3876 13.4213 14.0514 14.2725 11.8143 14.523C12.179 14.8401 12.4936 15.441 12.4936 16.3926C12.4936 17.7446 12.4773 18.8298 12.4773 19.1635C12.4773 19.4306 12.6596 19.748 13.1566 19.6478C17.1333 18.3121 20 14.5396 20 10.0826C20.0163 4.50694 15.526 0 10.0083 0Z"
                    fill="#526D82"
                  />
                </g>
                <defs>
                  <clipPath id="clip0_2_329">
                    <rect width="20" height="20" fill="white" />
                  </clipPath>
                </defs>
              </svg>
            </Link>
          </li>
        </ul>
      </section>
      <section className={Style.about} id="sectionAbout">
        <div className="container mx-auto flex row">
          <div className={Style.left}>
            <p
              data-aos="fade-up"
              data-aos-delay="200"
              data-aos-duration="1000"
              data-aos-anchor-placement="center-bottom"
              data-aos-easing="ease-in-out-back"
            >
              Experienced Web Developer adept in all stages of advanced web
              development. Demonstrated history of working in the marketing and
              advertising industry
            </p>
            <Link href="" id="aboutMeButton">
              <p>About Me</p>
            </Link>
          </div>
          <div className={Style.right}>
            <h1
              data-aos="fade-left"
              data-aos-delay="200"
              data-aos-duration="1000"
              data-aos-anchor-placement="center-bottom"
              data-aos-easing="ease-in-out-back"
              className={`${Style.motto} ${dmSherifDisplay.className}`}
            >
              “Effective is the ability to understand and manage something
              adaptively.”
            </h1>
          </div>
        </div>
      </section>
      <section className={Style.projectSection} id="sectionProject">
        <div className={`container mx-auto mb-32 ${Style.containerContent}`}>
          <div className={Style.divWrapBefore} id="wrapBefore"></div>
          <h1 className={`${Style.titleSection} ${poppins.className}`}>
            Project's
          </h1>
          <div className={Style.containerProject}>
            <div
              className={Style.project}
              data-aos="fade-left"
              data-aos-delay="200"
              data-aos-duration="1000"
              data-aos-anchor-placement="center-bottom"
              data-aos-easing="ease-in-out-back"
            >
              <Link href="">
                <h1 className={`${poppins.className} ${Style.title}`}>OLIN</h1>
                <p className={`${poppins.className} ${Style.position}`}>
                  Module Development
                </p>
              </Link>
            </div>
            <div
              className={Style.project}
              data-aos="fade-right"
              data-aos-delay="200"
              data-aos-duration="1000"
              data-aos-anchor-placement="center-bottom"
              data-aos-easing="ease-in-out-back"
            >
              <Link href="">
                <h1 className={`${poppins.className} ${Style.title}`}>
                  Sobat Indihome
                </h1>
                <p className={`${poppins.className} ${Style.position}`}>
                  Front-end Development
                </p>
              </Link>
            </div>
            <div
              className={Style.project}
              data-aos="fade-left"
              data-aos-delay="200"
              data-aos-duration="1000"
              data-aos-anchor-placement="center-bottom"
              data-aos-easing="ease-in-out-back"
            >
              <Link href="">
                <h1 className={`${poppins.className} ${Style.title}`}>
                  Aks.ess
                </h1>
                <p className={`${poppins.className} ${Style.position}`}>
                  Front-end Development
                </p>
              </Link>
            </div>
          </div>
        </div>
      </section>
      <section className={Style.footer} id="sectionFooter">
        <div id="contentFooter">
          <div
            className="container mx-auto"
            data-aos="fade-down"
            data-aos-delay="1000"
            data-aos-duration="1000"
            data-aos-anchor-placement="bottom-center"
            data-aos-easing="ease-in-out"
          >
            <p>
              Esse dolore mollit magna reprehenderit amet ea Lorem veniam
              laborum anim id dolore nulla. Voluptate sit incididunt
              reprehenderit qui consequat minim anim magna consequat labore.
              Tempor in proident laborum ipsum ad. Ipsum commodo proident ex
              nisi do nisi. Dolor occaecat labore sint eiusmod. Pariatur ea
              mollit qui adipisicing ea ex nisi velit et. Minim cillum sint enim
              qui consectetur commodo excepteur ad. Do ad nostrud labore
              adipisicing magna. Mollit mollit magna fugiat ipsum Lorem magna.
              Ut dolor nisi tempor officia ad qui sunt. Incididunt sint enim
              aliqua incididunt ea eu labore velit eu esse esse Lorem voluptate.
              Consectetur sint ipsum amet ut ipsum excepteur voluptate
              adipisicing minim veniam eiusmod enim ipsum eiusmod. Culpa veniam
              non adipisicing incididunt deserunt est. Consectetur deserunt
              deserunt veniam consequat aliqua velit ad id sit consequat culpa
              ullamco pariatur consectetur. Anim ad aute ut id commodo occaecat.
              Sint occaecat aliquip dolor nulla est elit eu cillum sunt
              consequat nostrud veniam ex mollit. Laborum mollit irure
              reprehenderit excepteur nisi. Qui cillum anim pariatur fugiat
              eiusmod officia sit veniam. Qui cupidatat eu eiusmod exercitation
              ex ea non labore reprehenderit adipisicing ipsum in. Non
              consectetur nulla veniam aliquip consectetur pariatur id nostrud
              esse aliquip duis ullamco exercitation cillum.
            </p>
          </div>
        </div>
      </section>
    </>
  );
}
