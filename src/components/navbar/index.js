"use client";
import Link from "next/link";
import Styles from "./index.module.scss";

export const metadata = {
  title: "Navbar",
};

export default function NavbarIndex() {
  return (
    <section className={Styles.navbar}>
      <div className={`container ${Styles.wrapper}`}>
        <div className={Styles.logo}>
          <Link href="">
            <span className={Styles.credits}>©</span>
            <span className={Styles.nickname}>famaulana</span>
            <span className={Styles.name}>Farhan Agung Maulana</span>
          </Link>
        </div>
        <ul className={Styles.navs}>
          <li>
            <Link href="">
              <div className={Styles.wrapText}>
                Project <span>Top 3 Project</span>
              </div>
            </Link>
          </li>
          <li>
            <Link href="">
              <div className={Styles.wrapText}>
                About <span>About Me</span>
              </div>
            </Link>
          </li>
          <li>
            <Link href="">
              <div className={Styles.wrapText}>
                Contact <span>Contact Me</span>
              </div>
            </Link>
          </li>
        </ul>
      </div>
    </section>
  );
}
